<?php
namespace App\Http\Controllers;

use App\Image;
use App\Jobs\ProcessImageWork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;

class ImageController extends Controller
{
    /**
     * Show Upload Form
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('upload_form');
    }

    /**
     * Upload Image
     *
     * @param  Request  $request
     * @return Response
     */
    public function process(Request $request)
    {
        //dd($request->all());
        // make db entry of that image
        $ids = [];
        if ($request->has('task_count') && is_int((int) $request->input('task_count')) && $request->input('task_count') < 100) {
            $cnt = 1;
            while ($cnt <= $request->input('task_count')) {
                $image = new Image;
                $image->org_path = $cnt . '_' . env('QUEUE_DRIVER') . '_queue diver_' . str_random(20);
                $image->save();
                $ids[] = $image->id;

                // defer the processing of the image
                ProcessImageWork::dispatch($image); // помещаем в очередь, указанную в настройках
                //ProcessImageWork::dispatch($image)->onQueue('emails'); //  помещаем в отдельную очередь
                //ProcessImageWork::dispatch($image)->delay(now()->addMinutes(10)); // задержка времени выполнения 10 мин
                // ProcessImageWork::dispatch($image)->onConnection('database'); // поместить процесс в отдельное соединение
                $cnt++;
            }

            return Redirect::to('image_index')->with('message', $cnt-1 . ' Tasks set successfully!')->with('ids', implode(',', $ids));
        } else {
            return Redirect::to('image_index')->with('message', 'Tasks set ERROR! Count tasks is not number or number is bigger than 100.');
        }
    }
}