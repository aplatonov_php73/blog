<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('image_index', 'ImageController@index')->name('image_index');
Route::post('image_process', 'ImageController@process')->name('image_process');
Route::get('/ajax_request/{job_id?}',function($job_id){
    $job = \App\Image::find($job_id);

    return Response::json($job);
});
